package ru.kombarov.tm.api.endpoint;

import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import ru.kombarov.tm.endpoint.SessionEndpointService;
import ru.kombarov.tm.endpoint.TaskEndpointService;
import ru.kombarov.tm.endpoint.UserEndpointService;

@Category(IntegrationTest.class)
public class ITaskEndpointTest extends TestCase {

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private Session session;

    @NotNull
    private final User test = new User();

    @NotNull
    private final Task task1 = new Task();

    @NotNull
    private final Task task2 = new Task();

    @NotNull
    private final Task task3 = new Task();

    public void testRemoveAllTasksByUserId() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.removeAllTasksByUserId(session);
        assertEquals(0, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testMergeTask() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.mergeTask(session, task1);
        assertEquals(1, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testPersistTask() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        assertEquals(1, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindOneTask() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.persistTask(session, task3);
        assertEquals(task2.getName(), taskEndpoint.findOneTask(session, task2.getId()).getName());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindTasksByName() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.persistTask(session, task3);
        assertEquals(task2.getId(), taskEndpoint.findTasksByName(session, task2.getName()).getId());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindAllTasksByUserId() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.persistTask(session, task3);
        assertEquals(3, taskEndpoint.findAllTasksByUserId(session).size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindOneTaskByUserId() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.persistTask(session, task3);
        assertEquals(test.getId(), taskEndpoint.findOneTaskByUserId(session, test.getId()).getUserId());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testRemoveTask() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        taskEndpoint.persistTask(session, task2);
        taskEndpoint.removeTask(session, task1.getId());
        assertEquals(1, taskEndpoint.findAllTasks(session).size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindTasksByPart() throws Exception_Exception {
        initTasks();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        taskEndpoint.persistTask(session, task1);
        assertEquals(1, taskEndpoint.findTasksByPart(session, "descr").size());
        taskEndpoint.removeAllTasks(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void initTasks() {
        test.setLogin("admin");
        test.setPassword("gffdhde");
        test.setRole(Role.ADMINISTRATOR);

        task1.setUserId(test.getId());
        task1.setName("Name1");
        task1.setDescription("description1");
        task1.setStatus(Status.PLANNED);

        task2.setUserId(test.getId());
        task2.setName("Name2");
        task2.setDescription("description2");
        task2.setStatus(Status.DONE);

        task3.setUserId(test.getId());
        task3.setName("Name3");
        task3.setDescription("description3");
        task3.setStatus(Status.IN_PROCESS);
    }
}