package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Session;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProject;
import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-select";
    }

    @NotNull
    @Override
    public String description() {
        return "Select the project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SELECT]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSession()));
        System.out.println("ENTER PROJECT NAME");
        final @Nullable Session session = serviceLocator.getSession();
        final @Nullable String projectName = input.readLine();
        final @Nullable String projectId = serviceLocator.getProjectEndpoint().findProjectByName(serviceLocator.getSession(), projectName).getId();
        printProject(serviceLocator.getProjectEndpoint().findOneProject(session, projectId));
        System.out.println("[OK]");
    }
}
