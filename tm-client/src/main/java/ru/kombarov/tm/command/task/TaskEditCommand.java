package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Task;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSession()));
        System.out.println("ENTER TASK NAME FOR EDIT");
        final @Nullable String nameAnotherTask = input.readLine();
        final @NotNull Task anotherTask = new Task();
        anotherTask.setName(nameAnotherTask);
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setDateFinish(parseStringToDate(input.readLine()));
        anotherTask.setId(serviceLocator.getTaskEndpoint().findTasksByName(serviceLocator.getSession(), nameAnotherTask).getId());
        anotherTask.setUserId(serviceLocator.getSession().getUserId());
        serviceLocator.getTaskEndpoint().mergeTask(serviceLocator.getSession(), anotherTask);
        System.out.println("[OK]");
    }
}
