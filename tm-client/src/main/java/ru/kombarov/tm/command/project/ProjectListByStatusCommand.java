package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.Status;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.util.EntityUtil;

public class ProjectListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list show by status";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects by status.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY STATUS]");
        System.out.println(Status.PLANNED.value() + ":");
        if (serviceLocator == null) throw new Exception();
        EntityUtil.printProjects(serviceLocator.getProjectEndpoint().sortProjectsByStatus(serviceLocator.getSession(), serviceLocator.getSession().getUserId()));
        System.out.println("[OK]");
    }
}
