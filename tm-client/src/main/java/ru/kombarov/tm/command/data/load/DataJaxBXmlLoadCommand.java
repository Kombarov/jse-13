package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataJaxBXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB xml load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by JaxB from XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB XML LOAD]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
