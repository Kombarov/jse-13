package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataFasterXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by FasterXML from XML format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML LOAD]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
