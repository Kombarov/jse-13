package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSession()));
        System.out.println("ENTER PROJECT NAME FOR REMOVE");
        final @Nullable String projectName = input.readLine();
        final @Nullable String projectId = serviceLocator.getProjectEndpoint().findProjectByName(serviceLocator.getSession(), projectName).getId();
        serviceLocator.getProjectEndpoint().removeProject(serviceLocator.getSession(), projectId);
        System.out.println("[OK]");
    }
}
