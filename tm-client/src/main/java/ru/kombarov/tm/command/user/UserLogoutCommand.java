package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Session;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        final @Nullable Session currentSession = serviceLocator.getSession();
        serviceLocator.getSessionEndpoint().removeSession(currentSession.getUserId(), currentSession.getId());
        serviceLocator.setSession(null);
        System.out.println("[OK]");
    }
}
