package ru.kombarov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.session.ISessionRepository;
import ru.kombarov.tm.api.session.ISessionService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final Bootstrap bootstrap;

    public SessionService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findOne(id);
    }

    @Override
    public void persist(final @Nullable Session session) throws Exception {
        if (session == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.persist(session);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(final @Nullable Session session) throws Exception {
        if (session == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.merge(session);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findOneByUserId(userId, id);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        try {
            sessionRepository.removeByUserId(userId, id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }
}
