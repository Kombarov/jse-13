package ru.kombarov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.user.IUserRepository;
import ru.kombarov.tm.api.user.IUserService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final Bootstrap bootstrap;

    public UserService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable User user) throws Exception {
        if (user == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.persist(user);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(final @Nullable User user) throws Exception {
        if (user == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.merge(user);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public User getUserByLogin(final @Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        return userRepository.getUserByLogin(login);
    }
}