package ru.kombarov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.project.IProjectRepository;
import ru.kombarov.tm.api.project.IProjectService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final Bootstrap bootstrap;

    public ProjectService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable Project project) throws Exception {
        if (project == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.persist(project);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(final @Nullable Project project) throws Exception {
        if (project == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.merge(project);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findAll();
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String userId,final  @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findOneByUserId(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public List<Project> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Project> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Project> sortByStatus(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.sortByStatus(userId);
    }

    @NotNull
    @Override
    public List<Project> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception {
        if (description == null || description.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        return projectRepository.findByPart(description, userId);
    }
}