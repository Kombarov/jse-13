package ru.kombarov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.task.ITaskRepository;
import ru.kombarov.tm.api.task.ITaskService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final Bootstrap bootstrap;

    public TaskService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.persist(task);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(final @Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.merge(task);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findOne(id);
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.remove(id);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAll();
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findOneByUserId(userId, id);
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        try {
            taskRepository.removeAllByUserId(userId);
            sqlSession.commit();
        }
        catch (SQLException e) {
            sqlSession.rollback();
        }
        finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Task findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public List<Task> getTasksByProjectId(final @Nullable String userId, final @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.getTasksByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public List<Task> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByDateStart(userId);
    }

    @NotNull
    @Override
    public List<Task> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByDateFinish(userId);
    }

    @NotNull
    @Override
    public List<Task> sortByStatus(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.sortByStatus(userId);
    }

    @NotNull
    @Override
    public List<Task> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception {
        if (description == null || description.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (bootstrap.getSqlSessionFactory() == null) throw new Exception();
        final @NotNull SqlSession sqlSession = bootstrap.getSqlSessionFactory().openSession();
        final @NotNull ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
        return taskRepository.findByPart(description, userId);
    }
}