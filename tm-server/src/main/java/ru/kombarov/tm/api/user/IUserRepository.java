package ru.kombarov.tm.api.user;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO `app_user` (`id`, `login`, `password`, `role`)" +
            " VALUES(#{id}, #{login}, #{password}, #{role})")
    void persist(final @NotNull User user) throws Exception;

    @Update("UPDATE `app_user` SET `login` = #{login}, `password` = #{password}" +
            "WHERE `id` = #{id}")
    void merge(final @NotNull User user) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_user`")
    List<User> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_user` WHERE `id` = #{id}")
    User findOne(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_user` WHERE `id` = #{id}")
    void remove(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_user`")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_user` WHERE `login` = #{login}")
    User getUserByLogin(final @NotNull String login) throws Exception;
}
