package ru.kombarov.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.User;

import java.util.List;

public interface IUserService {

    void persist(final @Nullable User user) throws Exception;

    void merge(final @Nullable User user) throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    @Nullable
    User findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll() throws Exception;

    @Nullable
    User getUserByLogin(final @Nullable String login) throws Exception;
}
