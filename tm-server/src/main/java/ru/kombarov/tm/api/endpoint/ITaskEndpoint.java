package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    void persistTask(final @Nullable Session session, final @Nullable Task task) throws Exception;

    @WebMethod
    void mergeTask(final @Nullable Session session, final @Nullable Task task) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findAllTasks(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeTask(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasks(final @Nullable Session session) throws Exception;

    @NotNull
    @WebMethod
    Task findTasksByName(final @Nullable Session session, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<Task> getTaskListByProjectId(final @Nullable Session session, final @Nullable String projectId) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findAllTasksByUserId(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTaskByUserId(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(final @Nullable Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByDateStart(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByDateFinish(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Task> sortTasksByStatus(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Task> findTasksByPart(final @Nullable Session session, final @Nullable String description) throws Exception;
}
