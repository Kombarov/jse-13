package ru.kombarov.tm.api.project;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO `app_project` (`id`, `userId`, `name`, `description`, `dateStart`, `dateFinish`, `status`)" +
            "VALUES (#{id}, #{userId}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{status})")
    void persist(final @NotNull Project project) throws Exception;

    @Update("UPDATE `app_project` SET `userId`= #{userId}, `name`= #{name}, `description`= #{description}," +
            "`dateStart`= #{dateStart}, `dateFinish`= #{dateFinish}, `status`= #{status} WHERE `id`= #{id}")
    void merge(final @NotNull Project project) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project`")
    List<Project> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `id` = #{id}")
    Project findOne(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_project` WHERE `id`= #{id}")
    void remove(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_project`")
    void removeAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId` = #{userId}")
    List<Project> findAllByUserId(final @NotNull String userId) throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `id` = #{id}")
    Project findOneByUserId(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws Exception;

    @Delete("DELETE FROM `app_project` WHERE `userId`= #{userId}")
    void removeAllByUserId(final @NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `name` = #{name}")
    Project findByName(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY `dateStart`")
    List<Project> sortByDateStart(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY `dateFinish`")
     List<Project> sortByDateFinish(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} ORDER BY FIELD(status, 'PLANNED', 'IN_PROCESS', 'DONE')")
    List<Project> sortByStatus(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_project` WHERE `userId`= #{userId} and `description` = #{description}")
    List<Project> findByPart(final @NotNull @Param("description") String description, final @NotNull @Param("userId") String userId) throws Exception;
}
