package ru.kombarov.tm.api.task;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO `app_task` (`id`, `projectId`, `userId`, `name`, `description`, `dateStart`, `dateFinish`, `status`) " +
            "VALUES (#{id}, #{projectId}, #{userId}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{status})")
    void persist(final @NotNull Task task) throws Exception;

    @Update("UPDATE `app_task` SET `projectId`= #{projectId}, `userId`= #{userId}, `name`= #{name}, `description`= #{description}," +
            "`dateStart`= #{dateStart}, `dateFinish`= #{dateFinish}, `status`= #{status} WHERE `id`= #{id}")
    void merge(final @NotNull Task task) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task`")
    List<Task> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `id` = #{id}")
    Task findOne(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_task` WHERE `id`= #{id}")
    void remove(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_task`")
    void removeAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId` = #{userId}")
    List<Task> findAllByUserId(@NotNull final String userId) throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `id` = #{id}")
    Task findOneByUserId(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws Exception;

    @Delete("DELETE FROM `app_task` WHERE `userId`= #{userId}")
    void removeAllByUserId(final @NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `name` = #{name}")
    Task findByName(final @NotNull @Param("userId") String userId, final @NotNull @Param("name") String name) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} and `projectId`= #{projectId}")
    List<Task> getTasksByProjectId(final @NotNull @Param("userId") String userId, final @NotNull @Param("projectId") String projectId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY `dateStart`")
    List<Task> sortByDateStart(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY `dateFinish`")
    List<Task> sortByDateFinish(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `userId`= #{userId} ORDER BY FIELD(status, 'PLANNED', 'IN_PROCESS', 'DONE')")
    List<Task> sortByStatus(final @NotNull @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM `app_task` WHERE `description` = #{description} and `userId`= #{userId}")
    List<Task> findByPart(final @NotNull @Param("description") String description, final @NotNull @Param("userId") String userId) throws Exception;
}
