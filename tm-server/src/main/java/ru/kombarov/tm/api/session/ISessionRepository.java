package ru.kombarov.tm.api.session;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Select("SELECT * FROM `app_session`")
    List<Session> findAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id`= #{id}")
    Session findOne(final @NotNull String id) throws Exception;

    @Insert("INSERT INTO `app_session` (`id`, `userId`, `timestamp`, `signature`, `role`) " +
            "VALUES(#{id}, #{userId}, #{timestamp}, #{signature}, #{role})")
    void persist(final @NotNull Session session) throws Exception;

    @Update("UPDATE `app_session` SET `userId` = #{userId}, `timestamp` = #{timestamp}, `signature` = #{signature}," +
            "`role` = #{role} WHERE `id`= #{id}" )
    void merge(final @NotNull Session session) throws Exception;

    @Delete("DELETE FROM `app_session` WHERE `id` = #{id}")
    void remove(final @NotNull String id) throws Exception;

    @Delete("DELETE FROM `app_session`")
    void removeAll() throws Exception;

    @Nullable
    @Select("SELECT * FROM `app_session` WHERE `id` = #{id} and `userId` = #{userId}")
    Session findOneByUserId(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws Exception;

    @Delete("DELETE FROM `app_session` WHERE `userId` = #{userId} and `id` = #{id}")
    void removeByUserId(final @NotNull @Param("userId") String userId, final @NotNull @Param("id") String id) throws Exception;
}
