package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void persistProject(final @Nullable Session session, final @Nullable Project project) throws Exception;

    @WebMethod
    void mergeProject(final @Nullable Session session, final @Nullable Project project) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjects(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeProject(final @Nullable Session session, final @NotNull String id) throws Exception;

    @WebMethod
    void removeAllProjects(final @Nullable Session session) throws Exception;

    @NotNull
    @WebMethod
    Project findProjectByName(final @Nullable Session session, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProjectsByUserId(final @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProjectByUserId(final @Nullable Session session, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(final @Nullable Session session) throws Exception;

    @NotNull
    @WebMethod
    List<Project> sortProjectsByDateStart(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Project> sortProjectsByDateFinish(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Project> sortProjectsByStatus(final @Nullable Session session, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findProjectsByPart(final @Nullable Session session, final @Nullable String description) throws Exception;
}
