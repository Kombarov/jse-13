package ru.kombarov.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public enum Status {

    PLANNED("PLANNED"),
    IN_PROCESS("IN_PROCESS"),
    DONE("DONE");

    @NotNull
    private final String name;

    @NotNull
    public String displayName() {
        return name;
    }
}

