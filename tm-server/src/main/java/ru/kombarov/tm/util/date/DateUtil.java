package ru.kombarov.tm.util.date;

import org.jetbrains.annotations.Nullable;

import java.sql.Date;

public class DateUtil {

    @Nullable
    public static Date parseDateToSQLDate(@Nullable final java.util.Date date) {
        if(date == null) return null;
        return new Date(date.getTime());
    }
}
