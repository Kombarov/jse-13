package ru.kombarov.tm.util.comparator.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.entity.Project;

import java.util.Comparator;
import java.util.Date;

public final class ProjectDateFinishComparator implements Comparator<Project> {

    @Override
    @SneakyThrows
    public int compare(final @NotNull Project o1, final @NotNull Project o2) {
        if (o1.getDateFinish() == null) throw new Exception();
        if (o2.getDateFinish() == null) throw new Exception();
        final @NotNull Date date1 = o1.getDateFinish();
        final @NotNull Date date2 = o2.getDateFinish();
        if (date1.after(date2)) return 1;
        if (date1.before(date2)) return -1;
        else return 0;
    }
}
