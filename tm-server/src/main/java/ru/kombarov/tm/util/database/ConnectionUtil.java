package ru.kombarov.tm.util.database;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.constant.DataConstant;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public final class ConnectionUtil {

    @NotNull
    public static Connection getConnection() throws Exception {
        final @NotNull Properties properties = new Properties();
        final InputStream propFile = new FileInputStream(DataConstant.FILE_PROPERTIES);
        properties.load(propFile);
        Class.forName(properties.getProperty("jdbcDriver"));
        return DriverManager.getConnection(properties.getProperty("db.host"),
                properties.getProperty("db.login"), properties.getProperty("db.password"));
    }
}
