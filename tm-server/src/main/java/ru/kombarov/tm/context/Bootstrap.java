package ru.kombarov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.api.project.IProjectRepository;
import ru.kombarov.tm.api.session.ISessionRepository;
import ru.kombarov.tm.api.task.ITaskRepository;
import ru.kombarov.tm.api.user.IUserRepository;
import ru.kombarov.tm.endpoint.ProjectEndpoint;
import ru.kombarov.tm.endpoint.SessionEndpoint;
import ru.kombarov.tm.endpoint.TaskEndpoint;
import ru.kombarov.tm.endpoint.UserEndpoint;
import ru.kombarov.tm.service.*;

import javax.sql.DataSource;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final TaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final UserService userService = new UserService(this);

    @Getter
    @NotNull
    private final SessionService sessionService = new SessionService(this);

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(sessionService, projectService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(sessionService, taskService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService, userService);

    @Getter
    @Nullable
    private SqlSessionFactory sqlSessionFactory;

    private SqlSessionFactory initSqlSessionFactory() throws Exception {
        @NotNull final String login = propertyService.getLogin();
        @NotNull final String url = propertyService.getUrl();
        @NotNull final String password = propertyService.getPassword();
        @NotNull final String driver = propertyService.getDriver();

        @NotNull
        final DataSource dataSource = new PooledDataSource(driver, url, login, password);

        @NotNull
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();

        @NotNull
        final Environment environment = new Environment("development", transactionFactory, dataSource);

        @NotNull
        final Configuration configuration = new Configuration(environment);

        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    public void init() throws Exception {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
        sqlSessionFactory = initSqlSessionFactory();
    }
}