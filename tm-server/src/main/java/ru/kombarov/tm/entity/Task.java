package ru.kombarov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.enumerated.Status;

import java.util.Date;

@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Getter
    @Setter
    @Nullable
    private String name;

    @Getter
    @Setter
    @Nullable
    private String projectId;

    @Getter
    @Setter
    @Nullable
    private String userId;

    @Getter
    @Setter
    @Nullable
    private String description;

    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateFinish;

    @Getter
    @Setter
    @NotNull
    private Status status;

    public Task(final @Nullable String name) {
        this.name = name;
        this.status = Status.PLANNED;
    }
}
