package ru.kombarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ITaskEndpoint;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.service.SessionService;
import ru.kombarov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private TaskService taskService;

    public TaskEndpoint() {
        super();
    }

    public TaskEndpoint(final @NotNull SessionService sessionService, final @NotNull TaskService taskService) {
        super(sessionService);
        this.taskService = taskService;
    }

    @Override
    @WebMethod
    public void persistTask(final @Nullable Session session, final @Nullable Task task) throws Exception {
        validateSession(session);
        taskService.persist(task);
    }

    @Override
    @WebMethod
    public void mergeTask(final @Nullable Session session, final @Nullable Task task) throws Exception {
        validateSession(session);
        taskService.merge(task);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasks(final @Nullable Session session) throws Exception {
        validateSession(session);
        return taskService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTask(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return taskService.findOne(id);
    }

    @Override
    @WebMethod
    public void removeTask(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        taskService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(final @Nullable Session session) throws Exception {
        validateSession(session);
        taskService.removeAll();
    }

    @NotNull
    @Override
    @WebMethod
    public Task findTasksByName(final @Nullable Session session, final @Nullable String name) throws Exception {
        validateSession(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectId(final @Nullable Session session, final @Nullable String projectId) throws Exception {
        validateSession(session);
        return taskService.getTasksByProjectId(session.getUserId(), projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasksByUserId(final @Nullable Session session) throws Exception {
        validateSession(session);
        return taskService.findAll(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Task findOneTaskByUserId(final @Nullable Session session, final @Nullable String id) throws Exception {
        validateSession(session);
        return taskService.findOne(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(final @Nullable Session session) throws Exception {
        validateSession(session);
        taskService.removeAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByDateStart(final @Nullable Session session, final @Nullable String userId) throws Exception {
        validateSession(session);
        return taskService.sortByDateStart(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByDateFinish(final @Nullable Session session, final @Nullable String userId) throws Exception {
        validateSession(session);
        return taskService.sortByDateFinish(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> sortTasksByStatus(final @Nullable Session session, final @Nullable String userId) throws Exception {
        validateSession(session);
        return taskService.sortByStatus(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findTasksByPart(final @Nullable Session session, final @Nullable String description) throws Exception {
        validateSession(session);
        return taskService.findByPart(description, session.getUserId());
    }
}
