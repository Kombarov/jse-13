package ru.kombarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ISessionEndpoint;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.service.SessionService;
import ru.kombarov.tm.service.UserService;
import ru.kombarov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

import static ru.kombarov.tm.constant.AppConstant.CICLE;
import static ru.kombarov.tm.constant.AppConstant.SALT;

@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    private UserService userService;

    public SessionEndpoint() {
        super();
    }

    public SessionEndpoint(final @NotNull SessionService sessionService, final @NotNull UserService userService) {
        super(sessionService);
        this.userService = userService;
    }

    @Override
    @Nullable
    @WebMethod
    public Session createSession(final @NotNull String login, final @NotNull String password) throws Exception {
        @Nullable final User user = userService.getUserByLogin(login);
        if(user == null) return null;
        if(!user.getPassword().equals(SignatureUtil.sign(password, SALT, CICLE))) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session, SALT, CICLE));
        sessionService.persist(session);
        return session;
    }

    @Override
    @WebMethod
    public void removeSession(final @Nullable String userId, final @Nullable String id) throws Exception {
        sessionService.remove(userId, id);
    }
}
